package handlers

import (
	"fmt"
	"net/http"

	m "gitlab.com/atarte/ads-app/models"
	v "gitlab.com/atarte/ads-app/views"
)

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Home menu")

	meteo := m.GetParisData()

	v.HomeView(w, meteo)
}
