# ADS-app

il va falloir tenter de voir comment modifier la version 
mais dabbord on regarde comment deployer l'application

test

kubectl port-forward svc/argocd-server -n argocd 8080:443

docker pull registry.gitlab.com/atarte/ads-app:latest
docker build -t ads-app:latest .
docker run --name ads-app -d -p 80:8080 registry.gitlab.com/atarte/ads-app:latest

##

Add to the runner config.toml this, or it will not be able to use docker inside the pipeline:
```
privileged = true
volumes = ["/cache","/var/run/docker.sock:/var/run/docker.sock"]
```

## TODO

([]) build number
([]) better interface

## Installer et configurer Argocd


Si aucune application n'est configurer dans Argocd alors créer en une. Sois en passant par l'interface web ou en executant la commande.

```
kubectl apply ads-app -n argocd
```

## Launch Argocd

This part is dedicated to the launching of Argocd if its at been correcly configure prior.

Start by launching minikube (make sure that docker is running).

```
minikube start
```

Open the connection to Argocd.

```
kubectl port-forward svc/argocd-server -n argocd 8080:443
```

Finay, if you're not on Linux, type this commande to open the connetion to the cluster.

```
minikube tunnel
```