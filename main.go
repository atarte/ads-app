package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	h "gitlab.com/atarte/ads-app/handlers"

	"github.com/gorilla/mux"
)

var (
	Version string = "0.1.3"
	Build   string = "abcd"
	Address string = "127.0.0.1"
	Port    string = "8888"
)

func main() {
	fmt.Println("ADS-APP")
	fmt.Println("version :", Version)
	fmt.Println("build :", Build)
	fmt.Println("address :", Address)
	fmt.Println("port :", Port)

	r := mux.NewRouter()

	staticDir := "/assets/"
	r.PathPrefix(staticDir).Handler(http.StripPrefix(staticDir, http.FileServer(http.Dir("."+staticDir))))

	r.HandleFunc("/", h.HomeHandler)

	srv_adr := Address + ":" + Port

	srv := &http.Server{
		Handler: r,
		Addr:    srv_adr,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	fmt.Println("Lauching server on : " + srv_adr)
	log.Fatal(srv.ListenAndServe())
}
