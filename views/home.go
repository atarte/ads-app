package views

import (
	"fmt"
	"html/template"
	"log"
	"net/http"

	m "gitlab.com/atarte/ads-app/models"
)

type HomeArgs struct {
	Data m.ApiData
}

func HomeView(w http.ResponseWriter, meteo m.ApiData) {
	fmt.Println("affichage des templates")

	h := HomeArgs{
		Data: meteo,
	}

	f := template.FuncMap{
		"GetCity":               meteo.GetCity,
		"GetTimeYear":           meteo.CurrentWeather.GetTimeYear,
		"GetTimeMonth":          meteo.CurrentWeather.GetTimeMonth,
		"GetTimeDay":            meteo.CurrentWeather.GetTimeDay,
		"GetTimeHours":          meteo.CurrentWeather.GetTimeHours,
		"GetTimeMinutes":        meteo.CurrentWeather.GetTimeMinutes,
		"GetAverageTemperature": meteo.CurrentTemperature.GetAverageTemperature,
		"GetAverageHumidity":    meteo.CurrentTemperature.GetAverageHumidity,
	}

	t := template.New("Home template").Funcs(f)

	t = template.Must(t.ParseFiles(
		"templates/layout.tmpl",
		"templates/home/home.tmpl",
	))

	err := t.ExecuteTemplate(w, "layout", h)
	if err != nil {
		log.Fatalf("Template execution: %s", err)
	}
}
