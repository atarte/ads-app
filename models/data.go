package models

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
)

type ApiData struct {
	CurrentWeather     CurrentWeather     `json:"current_weather"`
	CurrentTemperature CurrentTemperature `json:"hourly"`
}

var Cool string = "stress"

type CurrentWeather struct {
	Time          string  `json:"time"`
	Temperature   float64 `json:"temperature"`
	WeatherCode   int32   `json:"weathercode"`
	WindSpeed     float64 `json:"windspeed"`
	WindDirection float64 `json:"winddirection"`
}

type CurrentTemperature struct {
	// Time        []string  `json:"time"`
	// WindSpeed   []float32 `json:"windspeed_10m"`
	Temperature []float32 `json:"temperature_2m"`
	Humidity    []int     `json:"relativehumidity_2m"`
}

func (d *ApiData) GetCity() string {
	return "Paris"
}

func (d *CurrentWeather) GetTimeYear() string {
	return d.Time[:4]
}

func (d *CurrentWeather) GetTimeMonth() string {
	return d.Time[5:7]
}

func (d *CurrentWeather) GetTimeDay() string {
	return d.Time[8:10]
}

func (d *CurrentWeather) GetTimeHours() string {
	return d.Time[11:13]
}

func (d *CurrentWeather) GetTimeMinutes() string {
	return d.Time[14:16]
}

func getAverageArray[V int | float32](dataList []V) float32 {
	var sum V
	arrayLen := len(dataList)

	for num := range dataList {
		sum += V(num)
		// println(sum)
	}

	// println(arrayLen)
	return float32(sum / V(arrayLen))
}

func (d *CurrentTemperature) GetAverageTemperature() float64 {
	return math.Round(float64((getAverageArray(d.Temperature) / 1.8) - 32))
}

func (d *CurrentTemperature) GetAverageHumidity() float32 {
	return getAverageArray(d.Humidity)
}

func GetParisData() ApiData {
	response, err := http.Get("https://api.open-meteo.com/v1/forecast?latitude=48.85&longitude=2.35&current_weather=true&hourly=temperature_2m,relativehumidity_2m,windspeed_10m")
	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	var jsonData ApiData
	json.Unmarshal(responseData, &jsonData)

	// fmt.Println(jsonData)

	return jsonData
}
